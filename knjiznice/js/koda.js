var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function klicGenerirajPodatke(){

	$('#obvestilaGenerirej').text("");
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
	
	if(stPacienta == 1){
		ehrId = "7dad6fbf-8ae6-4414-aca8-0f81143e44be"
		var tezz = Math.round((Math.random() * 100) /2);
		var viss = Math.round((Math.random() * 100)/2+100);
		var siss = Math.round((Math.random() * 100) /2+60);
		var diss = Math.round((Math.random() * 100) /3+40);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>PJ Tucker: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 2){
		ehrId = "ef8e6d49-af85-49c6-ad49-2dd9a27161ba"
		var tezz = Math.round((Math.random() * 100) /2+40);
		var viss = Math.round((Math.random() * 200)/2+80);
		var siss = Math.round((Math.random() * 100) /2+110);
		var diss = Math.round((Math.random() * 100) /3+70);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>LB James: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 3){
		ehrId = "db3d3030-0a7f-4ad0-be50-71b9fcb19212"
		var tezz = Math.round((Math.random() * 100) /2+70);
		var viss = Math.round((Math.random() * 100)/2+140);
		var siss = Math.round((Math.random() * 100) /2+80);
		var diss = Math.round((Math.random() * 100) /3+65);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>JJ Redick: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}

  return ehrId;
}

function dodejPodatke(){
	var vis = $('#visina').val();
	var tez = $('#teza').val();
	var sis = $('#sistol').val();
	var dis = $('#diastol').val();
	var idd = $('#EHRID').val();
	dodajPodatkeEHR(idd,vis,tez,sis,dis);
}

function dodajPodatkeEHR(ehrId, visina, teza, sistol, distol){
	var sessid = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session":sessid
	}});
	var data = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2016-01-01",
		"vital_signs/height_length/any_event/body_height_length": visina,
        	"vital_signs/body_weight/any_event/body_weight": teza,
        	"vital_signs/blood_pressure/any_event/systolic": sistol,
        	"vital_signs/blood_pressure/any_event/diastolic": distol
	};
	var parampampam = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: 'jst'
	};
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parampampam),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function(odgovor){
			
	$('#obvestilaGenerirejj').text("Podatki dodani");
		},
		error: function(error){
	$('#obvestilaGenerirej').text("napaka");
			console.log("napaka"+error);
		}
	});
}

function dudejEHR(){
	dodejVBazo($('#dudejIme').val(),$('#dudejPriimek').val(),$('#dudejDatum').val());
}

function dodejVBazo(iime, ppriimek, ddatum){
	sessId = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session": sessId
	}});
	var odg = $.ajax({
		url: baseUrl + '/ehr',
		async: false,
		type: 'POST',
		success: function(data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: iime,
				lastNames: ppriimek,
				dateOfBirth: ddatum,
				partyAdditionalInfo: [{
					key: "ehrId", value: ehrId
				}]
		};
		$.ajax({
			url: baseUrl + "/demographics/party",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(partyData),
			success: function(party){
				$('#obvestilaDudej').text('Vaš EHR ID: ' + ehrId);
			},
			error: function(error){
				$('#obvestilaDudej').text('napaka');
			}
		});
	}
	});
}

function IzracunITM(){
sessionId = getSessionId();
	var	iddd = $('#EHRIDDD').val();
	console.log(iddd);
	$('#tolePobris').text("");
	if (!iddd || iddd.trim().length == 0) {
		$('#tolePobris').text("manjka ER-HR ID");
		return;
	}
	$.ajax({
            url: baseUrl + "/view/" + iddd + "/" + "blood_pressure?" + $.param({
                limit: 25
            }),
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
		success: function(tlk){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
		success: function(tez){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },	
		success: function(vis){
			prikaziPodatke(tlk,tez,vis);
		},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	
}

function prikaziPodatke(tlk,tez,vis) {
	var i = vis.length-1;
	var teza = tez[i].weight;
	var tmp = 0;
	var visina = vis[i].height;
	var sistolicni = tlk[i].systolic;
	var diastolicni = tlk[i].diastolic;
	var ITM = (teza/visina*visina);
	var tezamesage = "";
	var visinamessage = "";
	var sistolmessage = "";
	var diastolmessage = "";
	var ITMmessage= "";
	console.log(teza,visina,sistolicni,diastolicni);
	
	if(ITM < 19){
		tmp=ITM;
		$('#tolePobris').append("<p>Vaš ITM je prenizek. Svetujemo vam povečan vnos maščob (nepredelanih), ogljikovih hidratov in beljakovin.</p>");
		ITMmessage = "moj%20ITM%20je%20prenizek,%20";
	}
	
	if(ITM > 19 && ITM < 26){
		tmp=ITM;
		$('#tolePobris').append("<p>Vaš ITM je odličen. Nadaljujte z zdravim življenjskim slogom, uravnoteženo prehrano in veliko gibanja. Očitno imate postavo, ki vam jo drugi lahko zavidajo!</p>");
		ITMmessage = "moj%20ITM%20je%20odličen,%20";
	}
	
	if(ITM > 26){
		tmp=ITM;
		$('#tolePobris').append("<p>Vaš ITM je previsok. Jejte veliko sadja in zelenjave, ki so odličen nadomestek za nezdrave prigrizke. Priporočamo vam povečano fizično aktivnost. Rdeče meso zamenjajte za ribe.</p>");
		ITMmessage = "moj%20ITM%20je%20previsok,%20";
	}
	
	narisiGraf(tmp);
	$('#tvitni').append("<p></p><h3 style='text-align:center;'>TVITNI:</h3><p style='text-align:center;'><a class='twitter-share-button' href='https://twitter.com/intent/tweet?text="+ITMmessage+"%20%23ZdravoŽivljenje' data-size='large'><img src='https://static.addtoany.com/images/blog/tweet-button-2015.png' style='width:100px'></a></p>");
}

function narisiGraf(num){
$(function() {
	var data = [num];
        $('#tolePaDodej').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'ITM grafično - bližje sredini, bližje zdravemu življenju',
                x: -20 //center
            },
            xAxis: {
                categories: ['indeks telesne mase',
                ]
            },
            yAxis: {
		max: 40,
		min: 5,
                title: {
                    text: 'indeks'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'indeks telesne mase',
                data: data
            }]
        });
    });
}

$(window).load(function() {
    $("#vzorcnipacienti").change(function() {
        $('#EHRIDDD').val($('#vzorcnipacienti').val());
    });

});